Clearance.configure do |config|
  config.routes = true
  config.mailer_sender = "reply@example.com"
  config.rotate_csrf_on_sign_in = true
  config.allow_sign_up = false
end

# Override layout
Clearance::PasswordsController.layout "restricted"
Clearance::SessionsController.layout "restricted"
Clearance::UsersController.layout "restricted"
