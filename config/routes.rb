Rails.application.routes.draw do
  root 'reports#index'

  resources :reports, except: %w(new edit) do
    resources :tasks, controller: 'reports/tasks', only: %w(create update destroy)
  end

  resources :comments, only: %w(create update destroy)
  resource :profile, only: %w(show update)
end
