class ChangeProfileTimezone < ActiveRecord::Migration[5.1]
  def change
    rename_column :user_profiles, :timezone, :time_zone
    change_column :user_profiles, :time_zone, :string, default: 'UTC'
  end
end
