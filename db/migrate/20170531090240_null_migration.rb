class NullMigration < ActiveRecord::Migration[5.1]
  def up
    # These are extensions that must be enabled in order to support this database
    enable_extension "plpgsql"

    create_table "comments", force: :cascade do |t|
      t.text "message", null: false
      t.integer "commentable_id"
      t.string "commentable_type"
      t.integer "author_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.index ["author_id"], name: "index_comments_on_author_id"
      t.index ["commentable_type", "commentable_id"], name: "index_comments_on_commentable_type_and_commentable_id"
    end

    create_table "reports", force: :cascade do |t|
      t.string "state", default: "pending"
      t.bigint "user_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.index ["state"], name: "index_reports_on_state"
      t.index ["user_id"], name: "index_reports_on_user_id"
    end

    create_table "taggings", id: :serial, force: :cascade do |t|
      t.integer "tag_id"
      t.string "taggable_type"
      t.integer "taggable_id"
      t.string "tagger_type"
      t.integer "tagger_id"
      t.string "context", limit: 128
      t.datetime "created_at"
      t.index ["context"], name: "index_taggings_on_context"
      t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true
      t.index ["tag_id"], name: "index_taggings_on_tag_id"
      t.index ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context"
      t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy"
      t.index ["taggable_id"], name: "index_taggings_on_taggable_id"
      t.index ["taggable_type"], name: "index_taggings_on_taggable_type"
      t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type"
      t.index ["tagger_id"], name: "index_taggings_on_tagger_id"
    end

    create_table "tags", id: :serial, force: :cascade do |t|
      t.string "name"
      t.integer "taggings_count", default: 0
      t.index ["name"], name: "index_tags_on_name", unique: true
    end

    create_table "tasks", force: :cascade do |t|
      t.bigint "report_id"
      t.text "description"
      t.bigint "user_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.string "state", default: "todo"
      t.index ["report_id"], name: "index_tasks_on_report_id"
      t.index ["state"], name: "index_tasks_on_state"
      t.index ["user_id"], name: "index_tasks_on_user_id"
    end

    create_table "user_profiles", force: :cascade do |t|
      t.string "timezone"
      t.string "username"
      t.string "working_shift_start_time"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.bigint "user_id"
      t.index ["user_id"], name: "index_user_profiles_on_user_id"
    end

    create_table "users", force: :cascade do |t|
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.string "email", null: false
      t.string "encrypted_password", limit: 128, null: false
      t.string "confirmation_token", limit: 128
      t.string "remember_token", limit: 128, null: false
      t.index ["email"], name: "index_users_on_email"
      t.index ["remember_token"], name: "index_users_on_remember_token"
    end

    add_foreign_key "reports", "users"
    add_foreign_key "tasks", "reports"
    add_foreign_key "tasks", "users"
    add_foreign_key "user_profiles", "users"
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
