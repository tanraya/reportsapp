User.destroy_all

user = User.create!(email: 'demerest@gmail.com', password: 'BlackSwift73')
author = User.create!(email: 'johndoe@gmail.com', password: 'HelloWorld111')
report = user.reports.create!

task1 = report.tasks.create!(description: 'Need complete this', user: user, state: 'done')
task2 = report.tasks.create!(description: 'Need complete that', user: user, state: 'todo')
task3 = report.tasks.create!(description: 'Need complete that backlog 1', user: user, state: 'backlog')
task4 = report.tasks.create!(description: 'Need complete that backlog 2', user: user, state: 'backlog')

task1.comments.create!(message: 'Comment #1', author: author)
task1.comments.create!(message: 'Comment #2', author: author)
task1.comments.create!(message: 'Comment #3', author: author)

task3.comments.create!(message: 'Yet another Comment #1', author: author)
task3.comments.create!(message: 'Yet another Comment #2', author: author)
