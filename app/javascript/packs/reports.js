import React, { Component } from 'react'
import WebpackerReact from 'webpacker-react'
import { Provider } from 'react-redux'

import Report from './reports/components/report'
import configureStore from './reports/store/configure'

// Redux store
const store = configureStore()

// Actions
import * as actions from './reports/actions'

// Styles
import '!style-loader!css-loader!postcss-loader!./reports/styles.css';

// A Reports App
class ReportsApp extends Component {
  constructor(props) {
    super(props)

    // Prefill state
    store.dispatch(actions.prefillTasks(props.tasks))
    store.dispatch(actions.prefillReport(props.report))
  }

  render() {
    return (
      <Provider store={store}>
        <Report />
      </Provider>
    )
  }
}

WebpackerReact.setup({ReportsApp})
