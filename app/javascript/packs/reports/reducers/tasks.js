import {
  PREFILL_TASKS, CREATE_TASK_DONE, REMOVE_TASK_DONE, UPDATE_TASK_DONE, CREATE_COMMENT_DONE,
  UPDATE_COMMENT_DONE, REMOVE_COMMENT_DONE} from '../actions'

import initialState from './initial_state'
import deepcopy from 'deepcopy'

// Update redux storage state (tasks)
export default (state = initialState.tasks, action) => {
  const items = state.items.slice()
  let taskIndex, itemsCopy

  switch(action.type) {
    case PREFILL_TASKS:
      return { ...state, items: action.payload }
    case CREATE_TASK_DONE:
      return { ...state, items: items.concat(action.payload) }
    case REMOVE_TASK_DONE:
      return { ...state, items: items.filter(item => item.id !== action.payload) }
    case UPDATE_TASK_DONE:
      return { ...state, items: items.map(item => {
        return item.id === action.payload.id
          ? {...item, ...action.payload}
          : item
      }) }
    case CREATE_COMMENT_DONE:
      taskIndex = items.findIndex(item => item.id === action.payload.commentable_id)
      items[taskIndex].comments.push(action.payload)
      return {...state, items: items }
    case UPDATE_COMMENT_DONE:
      itemsCopy = deepcopy(items);
      taskIndex = items.findIndex(item => item.id === action.payload.taskId)

      itemsCopy[taskIndex].comments = itemsCopy[taskIndex].comments.map(comment => {
        return comment.id === action.payload.comment.id
          ? action.payload.comment
          : comment
      })

      return {...state, items: itemsCopy }
    case REMOVE_COMMENT_DONE:
      itemsCopy = deepcopy(items);
      taskIndex = items.findIndex(item => item.id === action.payload.taskId)

      itemsCopy[taskIndex].comments = itemsCopy[taskIndex].comments.filter(
        comment => comment.id !== action.payload.commentId
      )

      return {...state, items: itemsCopy }
    default:
      return state
  }
}
