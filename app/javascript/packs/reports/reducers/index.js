import {combineReducers} from 'redux'
import report from './report'
import tasks from './tasks'

export default combineReducers({
  report, tasks
})
