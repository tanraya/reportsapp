import {
  PREFILL_REPORT, PUBLISH_REPORT_DONE, REMOVE_TASK_DONE, CREATE_TASK_DONE, UPDATE_TASK_DONE,
  CREATE_COMMENT_DONE, UPDATE_COMMENT_DONE, REMOVE_COMMENT_DONE } from '../actions'
import initialState from './initial_state'

// Update redux storage state (report)
export default (state = initialState.report, action) =>{
  switch(action.type) {
    case PREFILL_REPORT:
      return { ...state, ...action.payload }
    case PUBLISH_REPORT_DONE:
      return { ...state, ...action.payload }

    // Restore report state on these actions
    case REMOVE_TASK_DONE:
    case CREATE_TASK_DONE:
    case UPDATE_TASK_DONE:
    case CREATE_COMMENT_DONE:
    case UPDATE_COMMENT_DONE:
    case REMOVE_COMMENT_DONE:
      return { ...state, state: 'pending' }
    default:
      return state
  }
}
