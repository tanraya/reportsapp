import React, { Component } from 'react'
import Moment from 'moment'

const MomentDate = (props) => {
  Moment.locale('en')
  return <span {...props}>{Moment(props.dateTime).format('LL')}</span>
}

export default MomentDate
