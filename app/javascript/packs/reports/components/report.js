import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import {Grid, Row, Col, Panel} from 'react-bootstrap'

// Actions
import { publishReport } from '../actions'

// Components
import Task from './task'
import MomentDate from './moment_date'

const AddTaskButton = ({bsStyle, ...props}) => {
  return <button className={`btn btn-xs btn-${bsStyle} pull-right`} title="Add new task" {...props}>
    <i className="glyphicon glyphicon-plus"></i>
  </button>
}

class Report extends Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: true,
      newTaskState: null
    }
  }

  componentDidMount() {
    this.setState({ loading: false })
  }

  handleAddTaskToBoard(state) {
    this.setState({ newTaskState: state })
  }

  handleSave() {
    this.setState({ newTaskState: null })
  }

  handlePublishReport(e) {
    e.preventDefault()
    this.props.dispatch(publishReport(this.props.report.id))
  }

  // Filter tasks by its state
  filterTasks(state) {
    return this.props.tasks.items.filter(x => x.state === state)
  }

  // Render list of tasks for a Board
  renderTasks(tasks) {
    return tasks.length > 0
      ? tasks.map(task => <Task key={task.id} task={task} report={this.props.report} />)
      : <div className="text-center"><em>No tasks</em></div>
  }

  renderNewTaskForm(state) {
    const task = {
      state: state,
      description: '',
      comments: []
    }

    if (this.state.newTaskState === state) {
      return <Task
        task={task}
        report={this.props.report}
        editing={true}
        onHideForm={this.handleSave.bind(this)} />
    }
  }

  render() {
    if (this.state.loading) {
      return <div className="loading">Loading...</div>
    }

    const tasks = {
      todo: this.filterTasks('todo'),
      done: this.filterTasks('done'),
      backlog: this.filterTasks('backlog')
    }

    const today = new Date();
    const dates = {
      yesterday: <MomentDate dateTime={today.setDate(today.getDate() - 1)} className="text-muted" />,
      today: <MomentDate dateTime={new Date()} className="text-muted" />
    }

    const titles = {
      todo: <h3>Yesterday {dates.yesterday} <AddTaskButton bsStyle="success" onClick={this.handleAddTaskToBoard.bind(this, 'todo')} /></h3>,
      done: <h3>Today {dates.today} <AddTaskButton bsStyle="warning" onClick={this.handleAddTaskToBoard.bind(this, 'done')} /></h3>,
      backlog: <h3>Backlog <AddTaskButton bsStyle="default" onClick={this.handleAddTaskToBoard.bind(this, 'backlog')} /></h3>
    }

    const publishButton = this.props.report.state === 'pending'
      ? <button
          className="btn btn-primary btn-sm"
          onClick={this.handlePublishReport.bind(this)}>Publish</button>
      : <span className="label label-success">completed</span>

    return (
      <div className="report">
        <header className="report-header container-fluid">
          <h4>
            Report #{this.props.report.id} by {this.props.report.author} at {this.props.report.created_at}.
            &nbsp;{publishButton}
          </h4>
        </header>
        <Grid bsClass="container-fluid">
          <Row>
            <Col md={4}>
              <Panel header={titles.todo} className="board" bsStyle="success">
                {this.renderTasks(tasks.todo)}
                {this.renderNewTaskForm('todo')}
              </Panel>
            </Col>
            <Col md={4}>
              <Panel header={titles.done} className="board" bsStyle="warning">
                {this.renderTasks(tasks.done)}
                {this.renderNewTaskForm('done')}
              </Panel>
            </Col>
            <Col md={4}>
              <Panel header={titles.backlog} className="board" bsStyle="default">
                {this.renderTasks(tasks.backlog)}
                {this.renderNewTaskForm('backlog')}
              </Panel>
            </Col>
          </Row>
        </Grid>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  tasks: state.tasks,
  report: state.report
})

export default connect(mapStateToProps)(Report)
