import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ButtonToolbar, DropdownButton, MenuItem } from 'react-bootstrap'

// Components
import TaskComments from './task/comments'

// Actions
import { createTask, updateTask, removeTask } from '../actions'

class Task extends Component {
  constructor(props) {
    super(props)

    this.state = {
      editing: (typeof props.editing === undefined ? false : props.editing),
      description: props.task.description
    }
  }

  updateDescription() {
    this.setState({ description: this.refs.editTask.value })
  }

  resetEditing(e) {
    if (e.key === 'Escape') { this.cancel(e) }
  }

  remove() {
    if (confirm('Remove this task forever?')) {
      this.props.dispatch(removeTask(this.props.report.id, this.props.task.id))
    }
  }

  edit(e) {
    e.preventDefault()
    this.setState({ editing: true })
  }

  save(e) {
    e.preventDefault()

    const saveTask = () => {
      return this.isPersisted()
        ? updateTask(this.props.report.id, this.props.task.id, this.refs.editTask.value)
        : createTask(this.props.report.id, this.refs.editTask.value, this.props.task.state)
    }

    this.props.dispatch(saveTask()).then(() => {
      this.setState({ editing: false })
      this.props.onHideForm()
    })
  }

  changeState(key) {
    this.props.dispatch(
      updateTask(this.props.report.id, this.props.task.id, this.state.description, key)
    )
  }

  cancel(e) {
    e.preventDefault()
    this.setState({ editing: false, description: this.props.task.description })
    this.props.onHideForm()
  }

  isPersisted() {
    return !!this.props.task.id
  }

  isValid() {
    return this.state.description.length > 0
  }

  renderEntry() {
    if (this.state.editing) {
      return <div className="task-edit">
        <div className="form-group">
          <textarea
            className="form-control"
            ref="editTask"
            rows="3"
            autoFocus={true}
            placeholder="Describe your task"
            onKeyDown={this.resetEditing.bind(this)}
            onChange={this.updateDescription.bind(this)}
            value={this.state.description}>
          </textarea>
        </div>

        <div className="btn-group">
          <button
            type="submit"
            className="btn btn-primary"
            disabled={!this.isValid()}
            onClick={this.save.bind(this)}>{this.isPersisted() ? 'Update' : 'Create'}</button>

          <button
            className="btn btn-default"
            onClick={this.cancel.bind(this)}>Cancel</button>
        </div>
      </div>
    }

    if (this.isPersisted()) {
      return <div className="task-body">
        <div className="task-author">{this.props.task.author} <em>at {this.props.task.created_at}</em></div>
        <div
          className="task-description"
          title="Double click to edit this task"
          onDoubleClick={this.edit.bind(this)}>{this.state.description}</div>
      </div>
    }
  }

  render() {
    const { task, task: { comments, state } } = this.props
    const taskButtons = this.isPersisted() && !this.state.editing && (
      <div className="task-buttons">
        <button className="task-edit" onClick={this.edit.bind(this)}>
          <i className="glyphicon glyphicon-pencil"></i>
        </button>

        <button className="task-remove" onClick={this.remove.bind(this)}>
          <i className="glyphicon glyphicon-remove"></i>
        </button>
      </div>
    )

    const stateButton = this.isPersisted() && !this.state.editing && (
      <ButtonToolbar>
        <DropdownButton bsStyle="default" bsSize="xsmall" noCaret title={state} id="changeState"
          onSelect={this.changeState.bind(this)}>

          {['todo', 'done', 'backlog'].map(curState =>
            state === curState
              ? null
              : <MenuItem key={curState} eventKey={curState}>{curState}</MenuItem>)}
        </DropdownButton>
      </ButtonToolbar>
    );

    return (
      <div className="task">
        {taskButtons}
        {stateButton}
        {this.renderEntry()}
        {this.isPersisted() && !this.state.editing && comments && <TaskComments task={task} comments={comments} />}
      </div>
    )
  }
}

export default connect((state) => ({
  report: state.report
}))(Task)
