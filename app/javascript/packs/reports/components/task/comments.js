import React, { Component } from 'react'
import { connect } from 'react-redux'

// Components
import TaskComment from './comment'

// Actions
import { createComment } from '../../actions'

class TaskComments extends Component {
  constructor(props) {
    super(props)

    this.state = {
      hidden: true,
      message: '',
      totalComments: props.comments.length
    }
  }

  toggleComments(e) {
    e.preventDefault()
    this.setState({ hidden: !this.state.hidden })
  }

  updateMessage() {
    this.setState({ message: this.refs.newComment.value })
  }

  addComment(e) {
    e.preventDefault()

    if (this.isValid()) {
      this.props.dispatch(createComment('Task', this.props.task.id, this.state.message))
        .then(() => { this.reset() })
    }
  }

  handleResetButton(e) {
    e.preventDefault()
    this.reset()
  }

  reset() {
    this.refs.newComment.value = ''
    this.setState({ message: '' })
  }

  isValid() {
    return this.state.message.length > 0
  }

  updateTotalComments(count) {
    this.setState({ totalComments: this.state.totalComments += count })
  }

  renderComments() {
    const { comments, task } = this.props

    return !this.state.hidden && (
      <div className="task-comments-toggle">
        <div className="task-comments">
          {comments.map(comment => <TaskComment
            key={comment.id}
            task={task}
            comment={comment}
            updateTotalComments={(count) => this.updateTotalComments(count)} />
          )}
        </div>

        <div className="task-new-comment">
          <div className="form-group">
            <textarea
              className="form-control"
              ref="newComment"
              placeholder="Write your thoughts"
              rows="2"
              onChange={this.updateMessage.bind(this)}>
            </textarea>
          </div>

          <div className="btn-group">
            <button
              type="submit"
              className="btn btn-primary"
              disabled={!this.isValid()}
              onClick={this.addComment.bind(this)}>Add Comment</button>

            {this.isValid() && <button
              className="btn btn-default"
              onClick={this.handleResetButton.bind(this)}>Reset</button>}
          </div>
        </div>
      </div>
    )
  }

  render() {
    const { comments } = this.props

    return (
      <div className="task-comments-wrapper">
        {this.renderComments()}

        <button className="task-comments-toggle-btn" onClick={this.toggleComments.bind(this)}>
          <i className="glyphicon glyphicon-comment"></i> {this.state.totalComments}
        </button>
      </div>
    )
  }
}

export default connect(null, null)(TaskComments)
