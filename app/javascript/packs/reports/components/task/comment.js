import React, { Component } from 'react'
import { connect } from 'react-redux'

// Actions
import { removeComment, updateComment } from '../../actions'

class TaskComment extends Component {
  constructor(props) {
    super(props)

    this.state = {
      editing: false,
      message: props.comment.message
    }
  }

  remove() {
    if (confirm('Remove this comment forever?')) {
      this.props.dispatch(removeComment(this.props.task.id, this.props.comment.id))
        .then(() => { this.props.updateTotalComments(-1) })
    }
  }

  edit(e) {
    e.preventDefault()
    this.setState({ editing: true })
  }

  updateMessage() {
    this.setState({ message: this.refs.editComment.value })
  }

  resetEditing(e) {
    if (e.key === 'Escape') { this.cancel(e) }
  }

  save(e) {
    e.preventDefault()

    if (this.isValid()) {
      this.props.dispatch(updateComment(this.props.task.id, this.props.comment.id, this.state.message))
        .then(() => {
          this.refs.editComment.value = ''
          this.setState({ editing: false, message: this.state.message })
        })
    }
  }

  cancel(e) {
    e.preventDefault()
    this.setState({ editing: false, message: this.props.comment.message })
  }

  isValid() {
    return this.state.message.length > 0
  }

  renderEntry() {
    if (this.state.editing) {
      return <div className="task-comment-edit">
        <div className="form-group">
          <textarea
            className="form-control"
            ref="editComment"
            rows="2"
            autoFocus={true}
            onChange={this.updateMessage.bind(this)}
            onKeyDown={this.resetEditing.bind(this)}
            value={this.state.message}>
          </textarea>
        </div>

        <div className="btn-group">
          <button
            type="submit"
            className="btn btn-primary"
            disabled={!this.isValid()}
            onClick={this.save.bind(this)}>Update</button>

          <button
            className="btn btn-default"
            onClick={this.cancel.bind(this)}>Cancel</button>
        </div>
      </div>
    } else {
      return <div className="task-comment-body">
        <div className="task-comment-author">{this.props.comment.author} <em>at {this.props.comment.created_at}</em></div>
        <div
          className="task-comment-message"
          title="Double click to edit this comment"
          onDoubleClick={this.edit.bind(this)}>{this.state.message}</div>
      </div>
    }
  }

  render() {
    const commentButtons = !this.state.editing && (
      <div className="task-comment-buttons">
        <button className="task-comment-edit" onClick={this.edit.bind(this)}>
          <i className="glyphicon glyphicon-pencil"></i>
        </button>

        <button className="task-comment-remove" onClick={this.remove.bind(this)}>
          <i className="glyphicon glyphicon-remove"></i>
        </button>
      </div>
    )

    return !this.state.removed && (
      <div className="task-comment">
        {commentButtons}
        {this.renderEntry()}
      </div>
    )
  }
}

export default connect((state, ownProps) => {
  const taskIndex = state.tasks.items.findIndex(task => task.id === ownProps.task.id)
  const comments = state.tasks.items[taskIndex].comments

  return {
    comments: comments
  }
})(TaskComment)
