import axios from 'axios'

// Constants
export const PREFILL_TASKS = 'PREFILL_TASKS'
export const REMOVE_TASK_DONE = 'REMOVE_TASK_DONE'
export const CREATE_TASK_DONE = 'CREATE_TASK_DONE'
export const UPDATE_TASK_DONE = 'UPDATE_TASK_DONE'

export const CREATE_COMMENT_DONE = 'CREATE_COMMENT_DONE'
export const UPDATE_COMMENT_DONE = 'UPDATE_COMMENT_DONE'
export const REMOVE_COMMENT_DONE = 'REMOVE_COMMENT_DONE'

export const PREFILL_REPORT = 'PREFILL_REPORT'
export const PUBLISH_REPORT_DONE = 'PUBLISH_REPORT_DONE'

////
// Tasks actions
export function publishReport(reportId) {
  return dispatch => {
    return axios.put(`/reports/${reportId}`)
      .then(response => {
        dispatch(publishReportDone(response.data))
      })
  }
}

export function publishReportDone(report) {
  return {
    type: PUBLISH_REPORT_DONE,
    payload: report
  }
}

////
// Tasks actions
export function prefillTasks(tasks) {
  return {
    type: PREFILL_TASKS,
    payload: tasks
  }
}

export function createTask(reportId, description, state) {
  return dispatch => {
    return axios.post(`/reports/${reportId}/tasks`, {
      description: description,
      state: state
    })
    .then(response => {
      dispatch(createTaskDone(response.data))
    })
  }
}

export function createTaskDone(task) {
  return {
    type: CREATE_TASK_DONE,
    payload: task
  }
}

export function updateTask(reportId, taskId, description, state = null) {
  const data = { description: description }
  if (state) { data.state = state }

  return dispatch => {
    return axios.put(`/reports/${reportId}/tasks/${taskId}`, data)
    .then(response => {
      dispatch(updateTaskDone(response.data))
    })
  }
}

export function updateTaskDone(task) {
  return dispatch => {
    dispatch({
      type: UPDATE_TASK_DONE,
      payload: task
    })
  }
}

export function removeTask(reportId, taskId) {
  return dispatch => {
    return axios.delete(`/reports/${reportId}/tasks/${taskId}`)
      .then(response => {
        dispatch(removeTaskDone(taskId))
      })
  }
}

export function removeTaskDone(taskId) {
  return {
    type: REMOVE_TASK_DONE,
    payload: taskId
  }
}

////
// Comment actions
export function createComment(commentableType, taskId, message) {
  return dispatch => {
    return axios.post(`/comments`, {
      message: message,
      commentable_type: commentableType,
      commentable_id: taskId
    })
    .then(response => {
      dispatch(createCommentDone(response.data))
    })
  }
}

export function createCommentDone(comment) {
  return {
    type: CREATE_COMMENT_DONE,
    payload: comment
  }
}

export function updateComment(taskId, commentId, message) {
  return dispatch => {
    return axios.put(`/comments/${commentId}`, {
      message: message
    })
    .then(response => {
      dispatch(updateCommentDone(response.data, taskId))
    })
  }
}

export function updateCommentDone(comment, taskId) {
  return dispatch => {
    dispatch({
      type: UPDATE_COMMENT_DONE,
      payload: { comment, taskId }
    })
  }
}

export function removeComment(taskId, commentId) {
  return dispatch => {
    return axios.delete(`/comments/${commentId}`)
      .then(response => {
        dispatch(removeCommentDone(taskId, commentId))
      })
  }
}

export function removeCommentDone(taskId, commentId) {
  return {
    type: REMOVE_COMMENT_DONE,
    payload: { taskId, commentId }
  }
}

////
// Report actions
export function prefillReport(report) {
  return {
    type: PREFILL_REPORT,
    payload: report
  }
}
