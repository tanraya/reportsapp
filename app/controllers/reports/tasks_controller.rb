class Reports::TasksController < ApplicationController
  before_action :find_report
  before_action :find_task, only: %w(update destroy)

  def create
    task = @report.tasks.build(
      description: params[:description],
      user: current_user,
      state: params[:state]
    )

    if task.save
      restore_report!
      render partial: 'tasks/task.json', locals: { task: task }
    else
      render json: { errors: task.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def update
    if @task.update(description: params[:description], state: params[:state] || @task.state)
      restore_report!
      render partial: 'tasks/task.json', locals: { task: @task }
    else
      render json: { errors: @task.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def destroy
    @task.destroy
    restore_report!

    head :ok
  end

  protected

  def find_report
    @report = Report.find(params[:report_id])
  end

  def find_task
    @task = @report.tasks.find(params[:id])
  end

  # Restore report state
  def restore_report!
    @report.restore! if @report.completed?
  end
end
