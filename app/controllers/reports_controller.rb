class ReportsController < ApplicationController
  before_action :find_report, only: %w(show update destroy)

  def index
    @reports = Report.desc.includes(:user).select(:id, :user_id, :state, :created_at)
  end

  def show
    @tasks = JbuilderTemplate.new(view_context) do |json|
      json.partial! "reports/show.json", tasks: @report.tasks.includes(:user, comments: :author)
    end.attributes!

    @concrete_report = JbuilderTemplate.new(view_context) do |json|
      json.partial! "reports/report.json", report: @report
    end.attributes!

    respond_to do |format|
      format.html
    end
  end

  def create
    report_service = CreateReportService.new(current_user)
    report = report_service.perform!
    redirect_to report
  end

  # Just complete report
  def update
    @report.complete! if @report.pending?
    render partial: 'reports/report.json', locals: { report: @report }
  end

  def destroy
    @report.destroy
    redirect_to reports_path
  end

  protected

  def find_report
    @report = Report.find(params[:id])
  end
end
