# User profile controller
class ProfilesController < ApplicationController
  before_action :find_profile, only: %w(show update)

  def show
  end

  def update
    if @profile.update(profile_params)
      respond_with @profile, location: profile_path
    else
      render 'show'
    end
  end

  protected

  def find_profile
    @profile = current_user.profile
  end

  def profile_params
    params.require(:user_profile).permit(
      :id, :username, :time_zone, :working_shift_start_time, user_attributes: [ :id, :email ]
    )
  end
end
