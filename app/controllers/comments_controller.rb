class CommentsController < ApplicationController
  before_action :find_comment, only: %w(update destroy)

  def create
    comment = obtain_commentable.comments.build(
      message: resource_params[:message],
      author: current_user
    )

    if comment.save
      restore_report!(comment)
      render partial: 'comments/comment.json', locals: { comment: comment }
    else
      render json: { errors: comment.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def update
    if @comment.update(message: params[:message])
      restore_report!(@comment)
      render partial: 'comments/comment.json', locals: { comment: @comment }
    else
      render json: { errors: @comment.errors.full_messages }, status: :unprocessable_entity
    end
  end

  # TODO: Restrict non-authors to delete comments?
  def destroy
    @comment.destroy
    restore_report!(@comment)
    head :ok
  end

  protected

  def obtain_commentable_class
    klass = resource_params[:commentable_type].to_s.safe_constantize
    fail "It is not a commentable object" if klass.nil? || !klass.new.respond_to?(:comments)
    klass
  end

  def obtain_commentable
    obtain_commentable_class.find(resource_params[:commentable_id])
  end

  def find_comment
    @comment = Comment.find(params[:id])
  end

  def resource_params
    params.require(:comment).permit(
      :message, :commentable_type, :commentable_id
    )
  end

  # Restore report state
  def restore_report!(comment)
    source = comment.commentable

    if [source.respond_to?(:report), source.report.is_a?(Report), source.report.completed?].all?
      source.report.restore!
    end
  end
end
