require "application_responder"

class ApplicationController < ActionController::Base
  include Clearance::Controller

  before_action :require_login
  around_action :set_time_zone, if: :current_user

  self.responder = ApplicationResponder
  respond_to :html

  private

  def set_time_zone(&block)
    Time.use_zone(current_user.profile.time_zone, &block)
  end
end
