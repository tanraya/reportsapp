# == Schema Information
#
# Table name: user_profiles
#
#  id                       :integer          not null, primary key
#  timezone                 :string
#  username                 :string
#  working_shift_start_time :string
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  user_id                  :integer
#

class User::Profile < ApplicationRecord
  belongs_to :user

  accepts_nested_attributes_for :user, reject_if: :all_blank
end
