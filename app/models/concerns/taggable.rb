module Taggable
  extend ActiveSupport::Concern

  included do
    acts_as_taggable
  end

  def taggables
    ActsAsTaggableOn::Tagging.joins(:tag)
                             .select('DISTINCT ON (taggings.taggable_id, taggings.taggable_type) taggings.*')
                             .where(tags: { name: tag_list })
                             .where.not(taggable_id: id, taggable_type: self.class.name)
                             .order('taggings.taggable_id, taggings.taggable_type, taggings.created_at DESC')
                             .map { |tagging| tagging.taggable }
  end
end
