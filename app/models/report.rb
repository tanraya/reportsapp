# == Schema Information
#
# Table name: reports
#
#  id         :integer          not null, primary key
#  state      :string           default("pending")
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# Report contains three kinds of lists: todo, done and backlog
class Report < ApplicationRecord
  include Workflow, Commentable

  belongs_to :user
  has_many :tasks, -> { order(created_at: :asc) }, dependent: :destroy

  scope :desc, -> { order(created_at: :desc) }
  scope :recent, -> { desc.first }

  workflow_column :state
  workflow do
    state :pending do
      event :complete, transitions_to: :completed
    end

    state :completed do
      event :restore, transitions_to: :pending
    end
  end
end
