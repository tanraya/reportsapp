# == Schema Information
#
# Table name: tasks
#
#  id          :integer          not null, primary key
#  report_id   :integer
#  description :text
#  user_id     :integer
#  hours_spent :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  state       :string           default("todo")
#

class Task < ApplicationRecord
  include Commentable, Taggable, Workflow

  belongs_to :report
  belongs_to :user

  validates :description, :state, presence: true

  workflow_column :state
  workflow do
    state :todo do
      event :to_done, transitions_to: :done
      event :to_backlog, transitions_to: :backlog
    end

    state :done do
      event :to_todo, transitions_to: :todo
      event :to_backlog, transitions_to: :backlog
    end

    state :backlog do
      event :to_done, transitions_to: :done
      event :to_todo, transitions_to: :todo
    end
  end
end
