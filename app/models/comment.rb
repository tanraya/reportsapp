# == Schema Information
#
# Table name: comments
#
#  id               :integer          not null, primary key
#  message          :text             not null
#  commentable_id   :integer
#  commentable_type :string
#  author_id        :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Comment < ActiveRecord::Base
  belongs_to :commentable, polymorphic: true
  belongs_to :author, class_name: 'User', required: true

  validates :message, :author, presence: true

  normalize_attributes :message
  before_save :sanitize_message

  default_scope { order(:created_at) }

  def to_s
    message
  end

  protected

  def sanitize_message
    self.message = ActionController::Base.helpers.strip_tags(message)
  end
end


