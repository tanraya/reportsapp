# == Schema Information
#
# Table name: users
#
#  id                 :integer          not null, primary key
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  email              :string           not null
#  encrypted_password :string(128)      not null
#  confirmation_token :string(128)
#  remember_token     :string(128)      not null
#

class User < ApplicationRecord
  include Clearance::User

  with_options dependent: :destroy do |o|
    o.has_one :profile, class_name: 'User::Profile'
    o.has_many :reports
    o.has_many :tasks
  end

  after_commit :create_profile!, on: :create

  def to_s
    profile.username || email
  end
end
