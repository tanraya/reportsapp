json.author(comment.author.to_s)
json.(comment, :id, :commentable_id, :commentable_type, :message)
json.created_at l(comment.created_at, format: :short)
