json.author(task.user.to_s)
json.(task, :id, :description, :state)
json.created_at l(task.created_at, format: :short)

json.comments task.comments do |comment|
  json.author(comment.author.to_s)
  json.created_at l(comment.created_at, format: :short)
  json.(comment, :id, :message, :commentable_id)
end

