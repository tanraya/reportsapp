module ApplicationHelper
  def report_state(report)
    html_class = report.pending? ? 'label-default' : 'label-success'
    content_tag :span, report.state, class: "label #{html_class}"
  end

  # Tmp fix for an issue with undefined method `react_component' in webpacker-react gem
  def react_component(component_name, props = {}, options = {})
    Webpacker::React::Component.new(component_name).render(props, options)
  end
end
