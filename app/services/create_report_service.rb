class CreateReportService
  attr_reader :user

  def initialize(user)
    @user = user
  end

  # Create daily report for user
  def perform!
    recent_report = user.reports.recent

    # If recent report was created today, just return it.
    if recent_report && today_report?(recent_report)
      recent_report
    # If there no recent reports at all, create new
    elsif !recent_report
      user.reports.create!
    # or create new report and copy incompleted tasks into it
    else
      report = user.reports.create!
      update_with_recent(report, recent_report)

      report
    end
  end

  protected

  def update_with_recent(report, recent_report)
    clone_tasks(report, recent_report.tasks.with_todo_state)
    clone_tasks(report, recent_report.tasks.with_backlog_state)
  end

  def today_report?(report)
    Time.now.in_time_zone.beginning_of_day == report.created_at.in_time_zone.beginning_of_day
  end

  def clone_tasks(report, tasks)
    tasks.each do |task|
      cloned_task = task.dup
      report.tasks << cloned_task

      # Clone comments
      task.comments.each do |comment|
        cloned_task.comments << comment
      end
    end
  end
end
